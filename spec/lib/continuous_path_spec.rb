require 'rails_helper'

RSpec.describe ContinuousPath do
  let(:instance)        { Builder.build(ContinuousPath) }
  let(:example_grid_1)  { [ [1,1,1,0], [1,1,0,0], [1,1,0,1], [1,0,0,1] ] }
  let(:example_grid_2)  { [ [1,1,1,0], [1,1,0,0], [0,1,0,1], [1,0,0,1] ] }
  let(:example_grid_3)  { [ [1,0,1,0,1], [1,1,1,1,1], [0,1,0,1,0], [1,1,1,1,1], [1,0,1,0,1] ] }
 
  it 'expects to evaluate the given examples on the test' do    
    
    expect(instance.find_min_path_with_coordinates(example_grid_1).first).to be_truthy
    expect(instance.find_min_path_with_coordinates(example_grid_2).first).to be_falsey
    expect(instance.find_min_path_with_coordinates(example_grid_3).first).to be_truthy
  end
end