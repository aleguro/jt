# Jericho Security test app

**Objective**

Develop a MVP-type web application that evaluates N x N grids of `0` and `1` values to
identify and visualize a continuous conductive path from the top to the bottom. A
conductive path is a contiguous sequence of the digit `1` that connects the top edge of the
grid to the bottom edge. The path may progress from one cell to another vertically (up or
down) or horizontally (left or right), but not diagonally. Each cell in the path must share a
common side with the next cell in the sequence. The app must also provide a UI for user
input and maintain and display a history of evaluations. 

### System Requirements

- Docker
- Make

### Build first time

```bash
make init
```

### Run the app

```
make start
```

## Use the app

Open [http://0.0.0.0:3000](http://0.0.0.0:3000) with your browser to see the result. The page auto-updates as you edit the file.

## Testing

Tests are written with [Rspec]

To run the tests:

```bash
make test
```

### Stop the app

```
make stop
```

### Un-install the app

```
make remove
```

### TODOs and improvements

This is a list of things that I'd improve 

- Move js vanilla code to Stimulus-JS
- Animate path 
- Improve randomize algorithm
- Use Background jobs for validating the grid 
- Use cable for updates
- Add rspec-capybara coverage for the UI
- Add Tailwind or any css framework and improve look and feel 