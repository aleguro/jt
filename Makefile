
init:
	docker-compose -f docker-compose.yml up -d redis postgresql
	docker-compose run app bundle exec rake db:drop db:create db:migrate db:seed
	docker-compose exec app rails webpacker:install
	make start

start:
	docker-compose up -d

stop:
	docker-compose down

test:
	docker-compose run app bundle exec rspec

remove:
	docker-compose down
	docker-compose rm -fsv

console:
	docker-compose run app rails c