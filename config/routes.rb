Rails.application.routes.draw do
  resources :evaluations
  
  root to: 'evaluations#index', only: [:index, :show, :create]
end
