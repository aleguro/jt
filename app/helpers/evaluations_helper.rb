module EvaluationsHelper
  
  ## Gral Grid Info
  def grid_info(grid)
    "Grid of #{grid.size} by #{grid.first.size}"
  end

  ## Hightlights cells that are part of the path
  def highlight_path_cell(row_index, col_index, path)
  
    return '' if path.empty?
    return '' if path.exclude? [row_index, col_index]

    return 'background-color: red;'
  end
end
