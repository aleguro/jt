class Evaluation < ApplicationRecord

  ## Validations
  validates :grid, presence: true

  ## Callbacks
  after_create :evaluate_continuous_path

  private

  ## Evaluates a grid after its creation 
  ##  TODO => Could use active-job for background process
  def evaluate_continuous_path

    cp_instance = ::Builder.build(ContinuousPath)

    self.evaluated = true
    self.path_presence, self.path = cp_instance.find_min_path_with_coordinates self.grid
  
    self.save()
  end
end
