module ContinuousPath
  
  # Function to find the minimum path and store its coordinates
  #  Input =>  
  #   [ [1,0,1,0,1], 
  #     [1,1,1,1,1], 
  #     [0,1,0,1,0], 
  #     [1,1,1,1,1], 
  #     [1,0,1,0,1] 
  #   ]
  #
  # Output =>
  #   true, [ [0, 0], [1, 0], [1, 1], [2, 1], [3, 1], [3, 0], [3, 2], [4, 2], [3, 3], [3, 4], [4, 4] ] 
  def find_min_path_with_coordinates(original_grid)

    grid = Marshal.load(Marshal.dump(original_grid))

    grid_length = grid.length

    visited = Array.new(grid_length) { Array.new(grid_length, false) } # Initialize visited array
    path    = []
    result  = []
  
    # Iterate through the first row and start DFS for each 1 cell in the first row
    grid_length.times do |col|
      if grid[0][col] == 1
        dfs(grid, visited, 0, col, [], result)
        break unless result.empty? # Break if the path is found
      end
    end

    result.each { |p| grid[p[0]][p[1]] = "*" } # Mark the path cells as '*'

    [ result.present?, result ] 
  end

  private

  def is_valid?(grid, visited, row, col)
    grid_length = grid.length
    row >= 0 && col >= 0 && row < grid_length && col < grid_length && grid[row][col] == 1 && !visited[row][col]
  end
  
  # Depth-first search (DFS) function to find the minimum path
  def dfs(grid, visited, row, col, path, result)
    # Define the possible movements: up, down, left, right
    row_moves = [-1, 1, 0, 0]
    col_moves = [0, 0, -1, 1]
  
    visited[row][col] = true # Mark the current cell as visited
    path << [row, col] # Add the current cell to the path
  
    grid_length = grid.length
    # If the current cell is in the last row, store the path coordinates
    if row == grid_length - 1
      result.replace(path)
      return
    end
  
    # Explore all possible directions
    4.times do |i|
      next_row = row + row_moves[i]
      next_col = col + col_moves[i]
  
      if is_valid?(grid, visited, next_row, next_col)
        dfs(grid, visited, next_row, next_col, path, result)
      end
    end
  
    path.pop # Remove the current cell from the path if it doesn't lead to the destination
  end
end