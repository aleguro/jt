## Seed the app with the samples provided in the requirements

evaluations = [ 
  { grid: [ [1,1,1,0], [1,1,0,0], [1,1,0,1], [1,0,0,1] ]  },
  { grid: [ [1,1,1,0], [1,1,0,0], [0,1,0,1], [1,0,0,1] ]  },
  { grid: [ [1,0,1,0,1], [1,1,1,1,1], [0,1,0,1,0], [1,1,1,1,1], [1,0,1,0,1] ] }
]

Evaluation.destroy_all
Evaluation.create!(evaluations)