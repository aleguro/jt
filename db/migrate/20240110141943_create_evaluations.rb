class CreateEvaluations < ActiveRecord::Migration[6.1]
  def change
    create_table :evaluations do |t|
      t.integer :grid, array: true
      t.boolean :path_presence, default: false
      t.boolean :evaluated, default: false
      t.integer :path, array: true

      t.timestamps
    end
  end
end
